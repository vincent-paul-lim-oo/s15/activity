alert('Hello World');
console.log('Hello World');

let firstNumber = parseInt(prompt('First number'));
let secondNumber = parseInt(prompt('Second number'));

if (firstNumber + secondNumber < 10) {
    let sum = firstNumber + secondNumber;
    console.log(sum);
    console.warn('The Sum is: ' + " " + sum);

} else if (firstNumber + secondNumber >= 10 && firstNumber + secondNumber <= 19) {
    let difference = firstNumber - secondNumber;
    console.log(difference);
    alert('The difference is: ' + " " + difference);

} else if (firstNumber + secondNumber >= 21 && firstNumber + secondNumber <= 29) {
    let product = firstNumber * secondNumber;
    console.log(product);
    alert('The product is :' + " " + product);

} else if (firstNumber + secondNumber >= 30) {
    let quotient = firstNumber / secondNumber;
    console.log(quotient);
    alert('The quotient is :' + " " + quotient);
}

//#5 Prompt for name & age
let name1 = prompt('Name');
let age = prompt('Age');

    if (name1 == "" || age == "") {
        console.log('Are you a time traveler?');

    } else if (name1 && age) {
        console.log("Your name is"+ " " + name1 + ", " + age + " " + "years of age.");

    } 

//#6
function isLegalAge(age) {
    if (age>=18) {
        console.log("You are of legal age!");
    } else {
        console.log("You are not allowed here!");
    }
}

isLegalAge(age);

//#7
switch (age) {
    case "18":
        console.log("You are now allowed to party.");
        break;
    case "21":
        console.log("You are now part of the adult society.");
        break;
    case "65":
        console.log("We thank you for your contribution to society.");
        break;
    default:
        console.log("Are you sure you're not an alien?");
        break;
}

//#8
//Ma'am Miah, please remove the comment to test try-catch-finally statement. (Comment was placed to avoid messing with the other parts of the code)

/* function isLegalAge(age) {

    try {
        test(isLegalAge(age))
    }

    catch (error) {
        console.warn(error.message)
    }

    finally {
        alert('OH NO!!!')
    }
}

isLegalAge() */