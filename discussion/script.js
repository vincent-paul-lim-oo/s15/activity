console.log("Hello World");

//Assignment Operators

//Basic Assignment Operator
let assignmentNumber = 8;

// Addition Assignment Operator
assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

//Shorthand
assignmentNumber += 2;
console.log(assignmentNumber) //12

assignmentNumber -= 2;
console.log(assignmentNumber); //10

assignmentNumber *= 2;
console.log(assignmentNumber); //20

assignmentNumber /= 2;
console.log(assignmentNumber); //10

//Arithmetic Operators
let x = 1397;
let y = 7831;

//Addition Operator (+)
let sum = x + y;
console.log(sum);

//Subtraction Operator (-)
let difference = x - y;
console.log(difference);

//Multiplication Operator  (*)
let product = x * y;
console.log(product);

//Division Operator  (/)
let quotient = y / x;
console.log(quotient);

//Modulo Operator
let remainder = y % x;
console.log(remainder);

//Multiplication Operators (PEMDAS)
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

/*


*/

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

pemdas = (1 + (2 - 3) * (4 / 5));
console.log(pemdas);

//Increment and Decrement

let z = 1;
//Pre-increment
let increment = ++z;
console.log(increment); //2
console.log(z); //2
//Post-Increment
increment = z++;
console.log(increment); //2
console.log(z); //3
//Pre-decrement
let decrement = --z;
console.log(decrement); //2
console.log(z); //2
//Post-decrement
decrement = z--;
console.log(decrement); //2
console.log(z); //1

//Type Coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

//Adding Boolean and Number
//true = 1
//false = 0
let numE = true + 1;
console.log(numE);
console.log(typeof numE);

let numF = false + 1;
console.log(numF);
console.log(typeof numF);

//Comparison Operators

let juan = 'juan';

//Equality Operator (==)
    //~checks whether the operands are equal or have the same content
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(1 === '1'); //false
console.log(0 == false); //true
console.log(juan == 'juan'); //true

//Inequality Operator (!=)
    //~checks whether the operands are not equal or have different content
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(juan != 'juan'); //false

//Strict Inequality Operator
    //~checks whether the operands are NOT equal or have the different content and also compares the data types of 2 values
console.log(1 !== 1);//false
console.log(1 !== "1");//true
console.log(0 !== false);//true
console.log(juan !== 'juan');//false

//Logical Operators

let isLegalAge = true;
let isRegistered = false;

//Logical AND operator (&&)
    //true && true = true
    //true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

//Logical OR Operator (|| - Double Pipe)
    //true || true = true
    //true || false = true

let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

//Logical NOT Operator (! - Exclamation Point)
    //returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet);

/*Control Structures*/
    /*if else statement*/
    /*switch statement*/

// if .. else statement
    /*
        syntax:

            if(condition) {
                statement
            } else {
                statement
            }


    */

//if statement
    //if statement can stand alone, will only execute if statement is true
    /*
        syntax:

            if(condition) {
                statement
            }
    */
let numG = -1

if (numG < 0) {
    console.log("HELLO");
}

if(false == "1") {
    console.log("Will not be printed");
}

//else if clause

let numH = 1;

if (numG > 0) {
    console.log("Hello");
} else if (numH > 0){
    console.log("World");
}

//else

if(numG > 0) {
    console.log("Hello");
} else if (numH == 0) {
    console.log("World");
} else {
    console.log("Again");
}

//Another example


//Return Function and Normal Function (2 ways to get Product) Start
function getProduct(a,b) {
    console.log(a * b);
}
getProduct(4,2)

function obtainProduct(x,y) {
    return x * y;
}
console.log(obtainProduct(2,2));
//Return Function and Normal Function (2 ways to get Product) End

//Quotient Start
//Return Function with Variable usage
function getQuotient(divisor,dividend) {
    return(dividend/divisor);
}

let divisor = 5;
let dividend = 25;

console.log(getQuotient(divisor, dividend));
//Normal Function with Variable usage
let divisorA = 5;
let dividendA = 25;

let quotientA = dividendA / divisorA
console.log(quotientA);

//Normal Function
function getQuotientA(divisorB, dividendB) {
    console.log(dividendB / divisorB);
}
getQuotientA(5,25);

//Return Function
function obtainQuotient(divisorC, dividendC) {
    return (dividendC / divisorC);
}
console.log((25, 5));
//Quotient End

console.log(5 === 6);
console.log(7 < 210);

//IF STATEMENT START
// "<" ">" "=" Relational Operator
let a = 10;
let b = 50;

if (a == 10 && b < 20) {
    console.log("GRANTED");
} else if ( a > 50) {
    console.log("ELSE IF!!!");
} else if ( b < 50) {
    console.log("ELSE IF");
} else {
    console.log("DENIED");
}

let gender = "Male";
let age = 18;

if (gender == "Male") {
    if (age == 18) {
        console.log("You are a " + gender + " and above the age limit!");
    }
    else {
        console.log("Sorry dude!");
    }
} else if (gender == "Female") {
    if (age >= 18) {
        console.log("You are a " + gender + " and above the age limit!");
    }
    else {
        console.log("Sorry lady!");
    }
} else {
    console.log("Can't figure out your gender!");
}
//IF STATEMENT END
//SWITCH STATEMENT START
/* let day;
switch (new Date().getDay()) {
    case 0:
        day = "Sunday";
        break;
    case 1:
        day = "Monday";
        break;
    case 2:
        day = "Tuesday";
        break;
    case 3:
        day = "Wednesday";
        break;
    case 4:
        day = "Thursday";
        break;
    case 5:
        day = "Friday";
        break;
    case 6:
        day = "Saturday";
        break;
}
console.log(day); */

let day;

switch (new Date().getDay()) {
    case 1:
        day = "Monday";
        break;
    case 2:
        day = "Tuesday";
        break;
    case 3:
        day = "Wednesday";
        break;
    case 4:
        day = "Thursday";
        break;
    case 5:
        day = "Friday";
        break;
    case 6:
        day = "Saturday";
        break;
    case 0:
        day = "Sunday";
        break;
    default:
        day = "I don't know?";
}

document.getElementById("test").innerHTML = "It is " + day;

//Mini Activity

let height = 152;

function obtainHeight(height) {
        if (height<150) {
        console.log("Did not pass the minimum height requirement");
    } else if (height>150) {
        console.log("Passed the minimum height requirement");
    }
}

obtainHeight(height);
obtainHeight(149);

//if, else if, else statement with FUNCTIONS

let message = 'No message';
console.log(message);

function determineTyphoonIntensity(windSpeed) {

    if (windSpeed < 30) {
        return 'Not a typhoon yet';
    } else if (windSpeed <= 61) {
        return 'Tropical depression detected';
    } else if (windSpeed >= 62 && windSpeed <= 88) {
        return 'Tropical storm detected';
    } else if (windSpeed >= 89 && windSpeed <= 117){
        return  'Severe tropical storm detected';
    } else {
        return 'Typhoon detected';
    }
}

message = determineTyphoonIntensity(70)
console.log(message);

if(message == 'Tropical storm detected') {
    console.warn(message);
}

//Truthy and Falsy
    //In JS, a truthy value is a value that is considered true when encountered in a boolean context.
    //Values are considered true unless defined otherwise.
    //Falsy values/exceptions for truthy
        //1. false
        //2. 0
        //3. -0
        //4. ""
        //5. null
        //6. undefined
        //7. NaN

if (true) {
    console.log('Truthy');
}

if (2) {
    console.log('Truthy');
}

if ([]) {
    console.log('Truthy')
}

if (false) {
    console.log('Falsy');
}

if (0) {
    console.log("Falsy");
}

if (undefined) {
    console.log("Falsy");
}

//Conditional Ternary Operator
    //Same with if else statement
    //3 Operands
    //The conditional ternary operator takes in three operands
        /*
            1. condition
            2. expression to execute if the condition is true
            3. expression to execute if the condition is false

            Syntax:
                (condition) ? ifTrue : isFalse
        */
       
let ternaryResult = (1 < 18) ? true : false
console.log(ternaryResult);

//Example
let name1;

function isOfLegalAge() {
    name1 = 'John!';
    return 'You are of legal age limit,';
}

function isUnderAge() {
    name1 = 'Jane!';
    return 'You are under the age limit,';
}

let age1 = parseInt(prompt("What is your age?"));
console.log(typeof age1)

let legalAge = (age1 > 18) ? isOfLegalAge() : isUnderAge();
console.log(legalAge + ' ' + name1);

//Switch Statement
/* 
    Syntax:
        switch(expression) {
            case value1:
                statement
                break;
            case value2:
                statement
                break;
            default:
                statement
                break;
        }
*/

let day1 = prompt("What day of the week is it today?").toLowerCase();
console.log(day1);
    
switch(day1) {
    case 'monday':
        console.log("The color of the day is red.");
        break;
    case 'tuesday':
        console.log("The color of the day is orange.");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow.");
        break;
    case 'thursday':
        console.log("The color of the day is green.");
        break;
    case 'friday':
        console.log("The color of the day is blue.");
        break;
    case 'saturday':
        console.log("The color of the day is indigo.");
        break;
    case 'sunday':
        console.log("The color of the day is violet.");
        break;
    default:
        console.log("Please input a valid day");
        break;
}

//Try-Catch-Finally Statement

function showIntensityAlert(windSpeed) {
        
    try {
        //attempts to execute a code
        alerat(determineTyphoonIntensity(windSpeed))
    }

    catch (error) {
        //handle the error
        console.warn(error.message)
    }


    finally {
        //regardless of the result, this will execute
        alert('Intensity updates will show new alert.')
    }
}

showIntensityAlert(56)